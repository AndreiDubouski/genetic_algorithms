import random
import numpy as np
import wx
import wx.xrc

from matplotlib.backends.backend_wxagg import FigureCanvasWxAgg as FigureCanvas
from matplotlib.backends.backend_wxagg import NavigationToolbar2WxAgg as NavigationToolbar
from mpl_toolkits import mplot3d

from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter

# For GUI constants
TITLE = "Lab2"
SIZE_X = 900
SIZE_Y = 800

TEXT_COUNT_POPULATION = "Размер популяции, шт."
TEXT_COUNT_GENS = "Количество генов, шт."
TEXT_PROBABILITY_COSING_OVER = "Вероятность кросинговера, %"
TEXT_PROBABILITY_MUTATE = "Вероятность мутации, %"
TEXT_COUNT_GENERATION = "Макс. количество поколений, шт."
TEXT_BEST = "Минимум"
TEXT_X1 = "X1"
TEXT_X2 = "X2"
TEXT_F = "F"

POP_SIZE = 20  # population size
DNA_SIZE = 10  # DNA length
CROSS_RATE = 0.6  # mating probability (DNA crossover)
MUTATION_RATE = 0.1  # mutation probability
N_GENERATIONS = 100  # кол-во возможных поколений
X_BOUND = [-50, 50]  # x upper and lower bounds
Z_BOUND = [-500, 800]  # z upper and lower bounds

N_COUNTS = 10  # условия выхода из цикла


def F(x1, x2):
    # Х в диапозоне -50 до 50
    return x1 * x1 + 2 * x2 * x2 - 0.3 * np.cos(3 * np.pi * x1) - 0.4 * np.cos(4 * np.pi * x2)


def decision(probability):
    """
    функция рандома с вероятностью - probability
    :param probability:
    :return:
    """
    return random.random() < probability


def split_list(a_list):
    """
    разбиение листа попалам
    :param a_list: список
    :return:
    """
    half = len(a_list) // 2
    return a_list[:half], a_list[half:]


def set_bit(v, index, x):
    """
    Set the index: th bit of v to 1 if x is truthy, else to 0, and return the new value.
    :param v:
    :param index:
    :param x:
    :return:
    """
    mask = 1 << index  # Compute mask, an integer with just bit 'index' set.
    v &= ~mask  # Clear the bit indicated by the mask (if x is False)
    if x: v |= mask  # If x was True, set the bit indicated by the mask.
    return v  # Return the result, we're done.


def generate_init_population(dna_size, pop_size):
    """
    Generates random population of gens
    :param pop_size:    кол-во особей в популяции
    :param dna_size:    koл-во генов на переменную
    :return:    список популяции
    """
    init_pop = []
    for _ in range(pop_size):
        style = []
        for _ in range(dna_size * 2):  # every bit in DNA
            style.append(random.randint(0, 1))
        random.shuffle(style)
        init_pop.append(style)
    print(init_pop)
    return init_pop


def translate_DNA(individ, DNA_size):
    """
    получение координат х1 и х2
    т.е. преобразование особи в точку t на графике цункций
    :param unit:
    :param count_gens: кол-во генов
    :return: в частном случае это число в пределах от -9.0 до 9.0
    """
    max_int = []
    for i in range(DNA_size):
        max_int.append(1)

    max_n = 0
    for i in range(DNA_size):
        position = DNA_size - i - 1
        bit = max_int[DNA_size - position - 1]
        max_n = set_bit(max_n, position, bit)
    # print("max value: ", max_n)
    x1, x2 = split_list(individ)
    x1_as_n = 0
    for i in range(DNA_size):
        position = DNA_size - i - 1
        bit = x1[DNA_size - position - 1]
        x1_as_n = set_bit(x1_as_n, position, bit)
    # print("x1", x1_as_n)
    x2_as_n = 0
    for i in range(DNA_size):
        position = DNA_size - i - 1
        bit = x2[DNA_size - position - 1]
        x2_as_n = set_bit(x2_as_n, position, bit)
    # print("x2", x2_as_n)
    x_1 = (X_BOUND[1] - X_BOUND[0]) * (x1_as_n / max_n)
    x_2 = (X_BOUND[1] - X_BOUND[0]) * (x2_as_n / max_n)
    # print(x_1, x_2)
    return x_1 + X_BOUND[0], x_2 + X_BOUND[0]


def mutate(individ, DNA_size):
    """
    Мутация (оператор инверсии) случайным образом выбираются две позиции в особи
    и далее производится обмен значениями генов между ними.
    :param individ:
    :param DNA_size:
    :return: мутант
    """
    start_position = random.randint(0, DNA_size * 2 - 1)
    end_position = random.randint(start_position, DNA_size * 2 - 1)
    print('start_position', start_position, 'end_position', end_position)
    mutant = []
    print("individ", individ)
    if start_position != end_position and individ:
        for i in range(len(individ)):
            if i < start_position:
                mutant.append(individ[i])
            elif i > end_position:
                mutant.append(individ[i])
            elif i >= start_position or i <= end_position:
                mutant.append(individ[end_position - i])
    else:
        mutant = individ
    print("mutant", mutant)
    return mutant


def crossover(parent_0, parent_1, DNA_size, cross_rate):
    """
    Однородный кроссинговер
    :param parent_0:
    :param parent_1:
    :param DNA_size:
    :param cross_rate:
    :return:
    """
    child_1, child_2 = [], []
    flag_1 = decision(probability=cross_rate)
    flag_2 = decision(probability=cross_rate)
    if flag_1:
        for i in range(DNA_size * 2):
            gen_1 = parent_1[i]
            gen_0 = parent_0[i]
            temp = decision(probability=0.5)
            if temp:
                child_1.append(gen_0)
            else:
                child_1.append(gen_1)

    if flag_2:
        for i in range(DNA_size * 2):
            gen_1 = parent_1[i]
            gen_0 = parent_0[i]
            temp = decision(probability=0.5)
            if temp:
                child_2.append(gen_0)
            else:
                child_2.append(gen_1)
    print("child_1", child_1, "child_2", child_2)

    return child_1, child_2


def selection_parents(all_population, DNA_size, pop_size):
    """
    Отбор родителей (селекция) оператор:
    -ранжирования (выбор лучших)
    :param all_population:
    :param DNA_size:
    :param pop_size:
    :return:
    """
    x_1, x_2, f = [], [], []
    unique_in_population = [list(x) for x in set(tuple(x) for x in all_population)]  # Uniqueness for list of lists
    for i in range(len(unique_in_population)):
        temp = translate_DNA(unique_in_population[i], DNA_size)
        x_1.append(temp[0])
        x_2.append(temp[1])
        f.append(F(temp[0], temp[1]))

    dict_all_population = {k: v for k, v in zip(f, unique_in_population)}
    # print(dict_all_population)
    dict_sorted = sorted(dict_all_population.items())
    # print(dict)
    future_parents = []
    count = 0
    for item in dict_sorted:
        if count < pop_size and len(f) >= count:
            future_parents.append(item[1])
        else:
            break
        count += 1
    print("future parents", future_parents)
    return future_parents


def get_children(population, DNA_size, cross_rate):
    """
    скрещивание всех со всеми, т.е. получаем детишек
    :param population: будущие родители
    :param DNA_size:
    :param cross_rate:
    :return: дети от всех
    """
    children = []
    for i in range(len(population)):
        for j in range(len(population)):
            ch_1, ch_2 = crossover(population[i], population[j], DNA_size, cross_rate)
            if ch_1:
                children.append(ch_1)
            if ch_2:
                children.append(ch_2)
    print("children", children)
    return children


def get_mutants(children, mutate_rate, DNA_size):
    """
    Мутация новых особей (работает оператор мутации)
    :param children:
    :param mutate_rate:
    :param DNA_size:
    :return:
    """
    mutants = []
    for child in children:
        temp = decision(probability=mutate_rate)
        if temp:
            mutant = mutate(child, DNA_size)
            if not mutant:
                print("I'm ugly")
            else:
                mutants.append(mutant)
    print("mutants", mutants)
    return mutants


class PlotPanel(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)
        self.figure = plt.figure(figsize=(6, 8))

        self.canvas = FigureCanvas(self, -1, self.figure)
        self.toolbar = NavigationToolbar(self.canvas)
        self.toolbar.Hide()

    def plot_3d(self):
        ax = self.figure.add_subplot(2, 1, 1, projection='3d')
        samples = 100
        x = np.linspace(X_BOUND[0], X_BOUND[1], samples)
        y = np.linspace(X_BOUND[0], X_BOUND[1], samples)
        X, Y = np.meshgrid(x, y)
        z = F(X, Y)
        ax.plot_surface(X, Y, z, cmap=cm.coolwarm, linewidth=0, antialiased=False, alpha=0.5)
        ax.set_xlabel('x 1')
        ax.set_ylabel('x 2')
        ax.set_zlabel('F')

        # точки
        x = np.linspace(X_BOUND[0], X_BOUND[1], samples)
        np.random.shuffle(x)
        y = np.linspace(X_BOUND[0], X_BOUND[1], samples)
        np.random.shuffle(y)
        z = F(x, y)
        N = 10  # количество
        color = np.random.randint(N, size=samples)
        ax.scatter(x, y, z, c=color)

        ax.grid(True)
        self.canvas.draw()

    def clear_plot(self):
        self.figure.clear()
        ax = self.figure.add_subplot(2, 1, 1, projection='3d')
        samples = 100
        x = np.linspace(X_BOUND[0], X_BOUND[1], samples)
        y = np.linspace(X_BOUND[0], X_BOUND[1], samples)
        X, Y = np.meshgrid(x, y)
        z = F(X, Y)
        ax.plot_surface(X, Y, z, cmap=cm.coolwarm, linewidth=0, antialiased=False, alpha=0.25)
        ax.set_xlabel('x 1')
        ax.set_ylabel('x 2')
        ax.set_zlabel('F')

        ax.grid(True)
        self.canvas.draw()

    def init_plot(self, population, DNA_size):
        self.figure.clear()
        ax = self.figure.add_subplot(2, 1, 1, projection='3d')
        samples = 100
        x = np.linspace(X_BOUND[0], X_BOUND[1], samples)
        y = np.linspace(X_BOUND[0], X_BOUND[1], samples)
        X, Y = np.meshgrid(x, y)
        z = F(X, Y)
        ax.plot_surface(X, Y, z, cmap=cm.coolwarm, linewidth=0, antialiased=False, alpha=0.35)
        ax.set_xlabel('x 1')
        ax.set_ylabel('x 2')
        ax.set_zlabel('F')

        # рисуем все точки
        x_, y_, z_ = [], [], []
        for pop in population:
            temp = translate_DNA(pop, DNA_size)
            x_.append(temp[0])
            y_.append(temp[1])
            z_.append(F(temp[0], temp[1]))
        N = 10  # количество colors
        color = np.random.randint(N, size=len(x_))
        ax.scatter(x_, y_, z_, c='k')

        # поиск наилучшего anf среднего
        f_min = min(z_)
        f_average = sum(z_) / float(len(z_))
        print("Fmin", f_min, "F_average", f_average)

        self.plot_best_and_average(f_min, f_average)

        i = z_.index(f_min)

        ax.grid(True)
        self.canvas.draw()
        return x_[i], y_[i], f_min, f_average

    def plot_best_and_average(self, f_min, f_average):
        ax = self.figure.add_subplot(2, 1, 2)
        # ax.hold(True)
        t_1 = -1.0
        t_2 = 1.0
        samples = 50
        # нагенеренные данные для логистической регресии
        t = np.linspace(t_1, t_2, samples)
        ax.scatter(0, f_average, c='y')
        ax.scatter(0, f_min, c='r')
        ax.axes.xaxis.set_ticklabels([])  # убрать нижнюю ось Х

        ax.grid(True)
        self.canvas.draw()


class GUI(wx.Frame):
    def __init__(self, parent):
        self.DNA_size = DNA_SIZE
        self.cross_rate = CROSS_RATE
        self.mutate_rate = MUTATION_RATE
        self.pop_size = POP_SIZE
        self.n_generation = N_GENERATIONS
        self.population = []
        self.end = False

        self.x1, self.x2, self.f, self.f_average = 0.0, 0.0, 0.0, 0.0

        no_resize = wx.SYSTEM_MENU | wx.SYSTEM_MENU | wx.CAPTION | wx.CLOSE_BOX
        wx.Frame.__init__(self, None, title=TITLE, size=wx.Size(SIZE_X, SIZE_Y), style=no_resize)

        self.sw = wx.SplitterWindow(self)
        self.SetMinSize((SIZE_X, SIZE_Y))
        self.SetMaxSize((SIZE_X, SIZE_Y))

        self.panel_main = wx.Panel(self.sw, style=wx.SUNKEN_BORDER)

        self.chart_panel = PlotPanel(self.sw)

        self.sw.SplitVertically(self.chart_panel, self.panel_main, 600)

        # кнопки, текст и поля
        self.status_bar = self.CreateStatusBar()
        self.status_bar.SetStatusText("init")

        self.init_but = wx.Button(self.panel_main, -1, "init", size=(260, 20), pos=(10, 10))
        self.run_but = wx.Button(self.panel_main, -1, "run", size=(260, 20), pos=(10, 40))
        self.clear_but = wx.Button(self.panel_main, -1, "clear", size=(260, 20), pos=(10, 70))

        # all labels
        self.label_0 = wx.StaticText(self.panel_main, label=TEXT_COUNT_POPULATION, pos=(10, 100))
        self.label_1 = wx.StaticText(self.panel_main, label=TEXT_COUNT_GENS, pos=(10, 130))
        self.label_2 = wx.StaticText(self.panel_main, label=TEXT_PROBABILITY_COSING_OVER, pos=(10, 160))
        self.label_3 = wx.StaticText(self.panel_main, label=TEXT_PROBABILITY_MUTATE, pos=(10, 190))
        self.label_4 = wx.StaticText(self.panel_main, label=TEXT_COUNT_GENERATION, pos=(10, 220))
        self.label_5 = wx.StaticText(self.panel_main, label=TEXT_BEST, pos=(120, 250))
        self.label_6 = wx.StaticText(self.panel_main, label=TEXT_X1, pos=(10, 280))
        self.label_7 = wx.StaticText(self.panel_main, label=TEXT_X2, pos=(10, 310))
        self.label_8 = wx.StaticText(self.panel_main, label=TEXT_F, pos=(10, 340))

        self.edit_count_pop = wx.TextCtrl(self.panel_main, size=(80, 20), pos=(190, 100))
        self.edit_gens = wx.TextCtrl(self.panel_main, size=(80, 20), pos=(190, 130))
        self.editProbabilityCrossingOver = wx.TextCtrl(self.panel_main, size=(80, 20), pos=(190, 160))
        self.edit_probability_mutate = wx.TextCtrl(self.panel_main, size=(80, 20), pos=(190, 190))
        self.editCountGenerations = wx.TextCtrl(self.panel_main, size=(80, 20), pos=(190, 220))

        self.edit_x1 = wx.TextCtrl(self.panel_main, size=(100, 20), pos=(170, 280))
        self.edit_x2 = wx.TextCtrl(self.panel_main, size=(100, 20), pos=(170, 310))
        self.edit_function = wx.TextCtrl(self.panel_main, size=(100, 20), pos=(170, 340))

        # Set event handlers
        self.init_but.Bind(wx.EVT_BUTTON, self.init)
        self.run_but.Bind(wx.EVT_BUTTON, self.run)
        self.clear_but.Bind(wx.EVT_BUTTON, self.clear)

        self.Show()

    def __del__(self):
        pass

    def draw(self, e):
        self.status_bar.SetStatusText("draw.. ")

    def init(self, e):
        self.status_bar.SetStatusText("init.. ")

        self.clear(e)

        self.pop_size = int(self.edit_count_pop.GetValue())
        self.DNA_size = int(self.edit_gens.GetValue())
        self.mutate_rate = float(self.edit_probability_mutate.GetValue())
        self.cross_rate = float(self.editProbabilityCrossingOver.GetValue())
        self.n_generation = int(self.editCountGenerations.GetValue())
        self.end = False

        self.population = generate_init_population(pop_size=self.pop_size, dna_size=self.DNA_size)
        x1, x2, f, _ = self.chart_panel.init_plot(self.population, self.DNA_size)

        # отображаем пользвателю лучшее хначение
        self.edit_x1.SetValue(str(x1))
        self.edit_x2.SetValue(str(x2))
        self.edit_function.SetValue(str(f))

    def run(self, e):
        self.status_bar.SetStatusText("processing.. ")
        old_f = 9999.9
        count = 0

        while self.n_generation != 0:
            mutate(self.population[0], self.DNA_size)
            crossover(self.population[0], self.population[1], self.DNA_size, self.cross_rate)
            children = get_children(self.population, self.DNA_size, self.cross_rate)
            mutants = get_mutants(children, self.mutate_rate, self.DNA_size)
            # new_generation
            self.population = selection_parents(self.population + children + mutants, self.DNA_size, self.pop_size)

            x1, x2, f, f_average = self.chart_panel.init_plot(self.population, self.DNA_size)
            self.x1, self.x2, self.f, self.f_average = x1, x2, f, f_average

            # отображаем пользвателю лучшее хначение
            self.edit_x1.SetValue(str(x1))
            self.edit_x2.SetValue(str(x2))
            self.edit_function.SetValue(str(f))

            # условия остановки или выхода из цикла
            if old_f != f_average:
                old_f = f_average
                count = 0
            else:
                if count == N_COUNTS:
                    break
                count += 1

            # уменьшаем итерации
            self.n_generation -= 1
            # отображаем пользвателю оставшиеся итерации
            self.status_bar.SetStatusText(str("generation: " + str(self.n_generation)))
            self.editCountGenerations.SetValue(str(self.n_generation))
            # time.sleep(0.1)

        self.show_message("End")

    def clear(self, e):
        """
        очистка переменных и стирание данных  в полях
        :param e:
        :return:
        """
        self.status_bar.SetStatusText("clear")
        self.pop_size = POP_SIZE
        self.DNA_size = DNA_SIZE
        self.mutate_rate = MUTATION_RATE
        self.cross_rate = CROSS_RATE  # вероятность скрещивание
        self.n_generation = N_GENERATIONS
        self.end = False

        self.edit_count_pop.SetValue(str(self.pop_size))
        self.edit_gens.SetValue(str(self.DNA_size))
        self.editProbabilityCrossingOver.SetValue(str(self.cross_rate))
        self.edit_probability_mutate.SetValue(str(self.mutate_rate))
        self.editCountGenerations.SetValue(str(self.n_generation))

        self.population = []
        self.chart_panel.clear_plot()

    def show_message(self, message):
        dlg = wx.MessageDialog(self, message, "Information", wx.OK)  # создаём всплывашку
        dlg.ShowModal()  # показываем окошко


if __name__ == '__main__':
    app = wx.App(False)
    frame = GUI(None).Show(True)
    app.MainLoop()
