# Циклический оператор кросинговера (СХ), файл lin105.tsp
import sys
import os
import numpy as np
import matplotlib.pyplot as plt
import wx
import wx.xrc
import random
from matplotlib.backends.backend_wxagg import FigureCanvasWxAgg as FigureCanvas
from matplotlib.backends.backend_wxagg import NavigationToolbar2WxAgg as NavigationToolbar
from mpl_toolkits import mplot3d

from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter

import time

# For GUI constants
TITLE = "Lab3"
SIZE_X = 900
SIZE_Y = 800

TEXT_COUNT_POPULATION = "Размер популяции, шт."
TEXT_COUNT_GENS = "Количество генов, шт."
TEXT_PROBABILITY_COSING_OVER = "Вероятность кросинговера, %"
TEXT_PROBABILITY_MUTATE = "Вероятность мутации, %"
TEXT_COUNT_GENERATION = "Количество поколений, шт."
TEXT_BEST = "Значение фитнесс функции"
TEXT_CITIES_LIST = "                             Список городов"

DIMENSION = "DIMENSION: "
EOF = "EOF\n"

POP_SIZE = 10  # population size
# POP_SIZE = 20  # population size
CROSS_RATE = 0.8  # mating probability (DNA crossover)
MUTATION_RATE = 0.2  # mutation probability
# MUTATION_RATE = 0.05  # mutation probability
N_GENERATIONS = 60  # кол-во возможных поколений
# N_GENERATIONS = 100  # кол-во возможных поколений

N_COUNTS = 25  # одно из условий выхода из цикла
best = [1,
        2,
        6,
        7,
        10,
        11,
        15,
        103,
        21,
        22,
        29,
        30,
        31,
        32,
        33,
        28,
        23,
        20,
        12,
        19,
        24,
        27,
        16,
        17,
        18,
        25,
        26,
        36,
        37,
        42,
        41,
        43,
        46,
        52,
        53,
        58,
        57,
        54,
        51,
        47,
        44,
        104,
        40,
        49,
        45,
        48,
        50,
        55,
        56,
        59,
        105,
        62,
        63,
        70,
        69,
        74,
        75,
        81,
        73,
        76,
        80,
        86,
        79,
        77,
        72,
        64,
        67,
        68,
        71,
        78,
        82,
        83,
        84,
        85,
        91,
        92,
        96,
        97,
        101,
        102,
        93,
        89,
        90,
        98,
        99,
        100,
        95,
        94,
        88,
        87,
        66,
        65,
        61,
        60,
        39,
        38,
        35,
        34,
        14,
        13,
        4,
        5,
        9,
        8,
        3]


def decision(probability):
    """
    функция рандома с вероятностью - probability
    :param probability:
    :return:
    """
    return random.random() < probability


def generate_init_population(cities, pop_size):
    """
    Generates random population of gens
    :param cities:          координаты городов
    :param pop_size:        кол-во особей в популяции
    :return:                список популяции
    """
    init_pop = []
    for i in range(pop_size):
        step = list(range(len(cities)))
        random.shuffle(step)
        init_pop.append(step)
    # print("population", init_pop)
    return init_pop


def get_pos_and_coordinate(cities):
    """
    получаем позиции, координаты по X и Y
    :param cities:
    :return:
    """
    pos, x, y = [], [], []
    for item in cities:
        pos.append(item[0])
        x.append(item[1])
        y.append(item[2])
    return pos, x, y


def mutate_0(individ, cities):
    """
    Случайным образом выбираются две позиции в особи и меняются местами
    и далее производится обмен значениями генов между ними.
    :param individ:     осбобь
    :param cities:      координаты городов
    :return:            мутант
    """
    pos, x, y = get_pos_and_coordinate(cities)
    eggs = list(range(len(individ)))
    random.shuffle(eggs)

    n_1, n_2 = eggs[:2]
    p_1, p_2 = individ.index(n_1), individ.index(n_2)
    # print('n_1: %d' % n_1, 'n_2: %d' % n_2)
    mutant = []
    # print("individ", individ)
    if n_1 != n_2 and individ:
        mutant = list(individ)
        mutant[p_1] = n_2
        mutant[p_2] = n_1
    else:
        mutant = list(individ)
    # print("mutant", mutant)
    return mutant

def mutate(individ, cities):
    """
    Случайным образом выбираются две позиции в особи и меняются местами
    и далее производится обмен значениями генов между ними.
    :param individ:     осбобь
    :param cities:      координаты городов
    :return:            мутант
    """
    mutant = list(individ)
    random.shuffle(mutant)
    # print("mutant", mutant)
    return mutant


def crossover(parent_0, parent_1, cross_rate):
    """
    Циклический ОК (СХ)
    :param parent_0:
    :param parent_1:
    :param cross_rate:
    :return:
    """
    if parent_0 == parent_1:
        # print("parents identical")
        child_1, child_2 = list(parent_0), list(parent_0)
    else:
        flag = decision(probability=cross_rate)
        if flag:
            mask = [None] * len(parent_0)  # фактическая маска для скрещивания
            child_1, child_2 = [None] * len(parent_0), [None] * len(parent_0)

            used_DNA = []
            i = 0
            counter = 0
            while True:

                i_pos_1 = parent_0[i]
                mask[i] = i_pos_1
                used_DNA.append(i_pos_1)
                i_pos_2 = parent_1[i_pos_1]
                if parent_0[i_pos_2] in used_DNA:
                    break
                else:
                    i = i_pos_2
                counter += 1
                if counter > 1000:
                    break

            for i in range(len(mask)):
                if mask[i] != None:
                    child_1[i] = int(parent_0[i])
                    child_2[i] = int(parent_1[i])
                else:
                    child_1[i] = int(parent_1[i])
                    child_2[i] = int(parent_0[i])
        else:
            child_1, child_2 = [], []

        # удаляем уродцев, т.е.
        counter = {}
        for elem in child_1:
            counter[elem] = counter.get(elem, 0) + 1
        # print(counter.values())       # дубликаты значений в генофонде ребёнка

        for item in counter.values():
            if item > 1:
                # print("I am ugly")
                child_1 = []
                break

        counter = {}
        for elem in child_2:
            counter[elem] = counter.get(elem, 0) + 1
        # print(counter.values())       # дубликаты значений в генофонде ребёнка
        for item in counter.values():
            if item > 1:
                # print("I am ugly")
                child_2 = []
                break

                # print("child_1", child_1, "child_2", child_2)
    return child_1, child_2


def get_children(population, cross_rate):
    """
    скрещивание всех со всеми, т.е. получаем детишек
    :param population: будущие родители
    :param cross_rate:
    :return: дети от всех
    """
    children = []
    for i in range(len(population)):
        for j in range(len(population)):
            ch_1, ch_2 = crossover(population[i], population[j], cross_rate)
            if ch_1:
                children.append(ch_1)
            if ch_2:
                children.append(ch_2)
    # print("children", children)
    return children


def get_mutants(children, mutate_rate, cities):
    """
    Мутация новых особей (работает оператор мутации)
    :param children:
    :param mutate_rate:
    :param cities:
    :return:
    """
    mutants = []
    for i in range(len(children)):
        temp = decision(probability=mutate_rate)
        if temp:
            mutant = mutate(children[i], cities)
            if not mutant:
                print("I'm ugly")
            else:
                mutants.append(mutant)
    # print("mutants", mutants)
    return mutants


def get_mutants_0(children, mutate_rate, cities):
    """
    Мутация новых особей (работает оператор мутации)
    :param children:
    :param mutate_rate:
    :param cities:
    :return:
    """
    mutants = []
    for i in range(len(children)):
        temp = decision(probability=mutate_rate)
        if temp:
            mutant = mutate_0(children[i], cities)
            if not mutant:
                print("I'm ugly")
            else:
                mutants.append(mutant)
    # print("mutants", mutants)
    return mutants


def selection_parents_0(self, all_population, cities, pop_size):
    """
    Отбор родителей выбор лучших
    :param self:
    :param all_population:
    :param cities:
    :param pop_size:
    :return:
    """
    f = self.calculate_fitness_F(all_population, cities)
    f_n = list(range(len(all_population)))
    eggs = dict()
    for i in range(len(all_population)):
        eggs[f_n[i]] = f[i]

    s_eggs = dict(sorted(eggs.items(), key=lambda value: value[1]))
    best_n = []
    counter = 0
    for key, value in s_eggs.items():
        if counter < pop_size:
            best_n.append(key)
        else:
            break
        counter += 1

    future_parents = []
    for i in range(len(all_population)):
        if i in best_n:
            future_parents.append(all_population[i])

    # print("future parents", future_parents)
    return future_parents


def selection_parents(self, all_population, cities, pop_size):
    """
    Отбор родителей выбор лучших
    :param self:
    :param all_population:
    :param cities:
    :param pop_size:
    :return:
    """
    f = self.calculate_fitness_F(all_population, cities)
    f_n = list(range(len(all_population)))
    eggs = dict()
    for i in range(len(all_population)):
        eggs[f_n[i]] = f[i]

    s_eggs = dict(sorted(eggs.items(), key=lambda value: value[1]))
    best_n = []
    counter = 0
    for key, value in s_eggs.items():
        if counter < pop_size:
            best_n.append(key)
        else:
            break
        counter += 1

    future_parents = []
    future_parents.append(all_population[0])    #лучшая особь будет
    random_items = random.choices(population=all_population, k=pop_size-1)

    for i in range(len(all_population)):
        if i in best_n:
            future_parents.append(all_population[i])

    # print("future parents", future_parents)
    return future_parents


class PlotPanel(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)
        self.figure = plt.figure(figsize=(6, 8))

        self.canvas = FigureCanvas(self, -1, self.figure)
        self.toolbar = NavigationToolbar(self.canvas)
        self.toolbar.Hide()

    def plot(self, cities):
        self.figure.clear()
        ax = self.figure.add_subplot(2, 1, 1)
        # ax.hold(True)
        t_1 = 0
        t_2 = len(cities) + 1
        pos, X, Y = [], [], []
        for item in cities:
            pos.append(item[0])
            X.append(item[1])
            Y.append(item[2])
        Xmax = max(X)
        Ymax = max(Y)

        ax.set_xlim(0, right=(Xmax + 45))
        ax.set_ylim(0, (Ymax + 34))

        ax.set_xlabel('x')
        ax.set_ylabel('y')

        for item in cities:
            # ax.scatter(item[1], item[2], c='b')
            area = (30 * np.random.rand(50)) ** 2
            ax.scatter(item[1], item[2], s=50, c='b', alpha=0.4)

        # обрисовка линии
        # ax.arrow(X[0], Y[0], X[1] - X[0], Y[1] - Y[0], width=0.02, color='red', head_length=0.0, head_width=0.0)

        # ax.grid(True)     # рисуем сетку
        self.canvas.draw()

    def plot_line(self, x1, x2, y1, y2):
        ax = self.figure.add_subplot(2, 1, 1)
        ax.arrow(x1, y1, x2 - x1, y2 - y1, width=0.02, color='red', head_length=0.0, head_width=0.0)
        self.canvas.draw()

    def plot_way(self, cities, individ):
        pos, x, y = get_pos_and_coordinate(cities)
        for i in range(len(individ) - 1):
            n_A, n_B = individ[i], individ[i + 1]
            self.plot_line(x[n_A], x[n_B], y[n_A], y[n_B])
            # self.canvas.draw()

    def plot_best_and_average(self, f_min, f_average):
        ax = self.figure.add_subplot(2, 1, 2)
        #        ax.hold(True)
        t_1 = -1.0
        t_2 = 1.0
        samples = 5
        # нагенеренные данные для логистической регресии
        t = np.linspace(t_1, t_2, samples)
        ax.scatter(0, f_average, c='y')
        ax.scatter(0, f_min, c='r')
        ax.axes.xaxis.set_ticklabels([])  # убрать нижнюю ось Х

        ax.grid(True)
        self.canvas.draw()

    def iterate_plot(self, cities, individ):
        self.figure.clear()
        ax = self.figure.add_subplot(2, 1, 1)
        # ax.hold(True)
        pos, X, Y = [], [], []
        for item in cities:
            pos.append(item[0])
            X.append(item[1])
            Y.append(item[2])
        Xmax = max(X)
        Ymax = max(Y)

        ax.set_xlim(0, right=(Xmax + 45))
        ax.set_ylim(0, (Ymax + 34))

        ax.set_xlabel('x')
        ax.set_ylabel('y')

        for item in cities:
            ax.scatter(item[1], item[2], s=50, c='b', alpha=0.4)

        # обрисовка линий
        self.plot_way(cities, individ)

        # ax.grid(True)       # рисуем сетку
        self.canvas.draw()

    def plot_best_and_average_continuously(self, F_best, F_mean):
        ax = self.figure.add_subplot(2, 1, 2)

        i = list(range(len(F_best)))
        ax.scatter(i, F_best, c='r')
        ax.scatter(i, F_mean, c='y')

        ax.axes.xaxis.set_ticklabels([])  # убрать нижнюю ось Х

        # ax.grid(True)             # рисуем сетку
        self.canvas.draw()


class GUI(wx.Frame):
    def __init__(self, parent):
        self.cross_rate = CROSS_RATE
        self.mutate_rate = MUTATION_RATE
        self.pop_size = POP_SIZE
        self.n_generation = N_GENERATIONS
        self.population = []
        self.end = False
        self.cities = []
        self.F_min = []
        self.F_mean = []

        self.x1, self.x2, self.f, self.f_average = 0.0, 0.0, 0.0, 0.0

        no_resize = wx.SYSTEM_MENU | wx.SYSTEM_MENU | wx.CAPTION | wx.CLOSE_BOX
        wx.Frame.__init__(self, None, title=TITLE, size=wx.Size(SIZE_X, SIZE_Y), style=no_resize)

        self.sw = wx.SplitterWindow(self)
        self.SetMinSize((SIZE_X, SIZE_Y))
        self.SetMaxSize((SIZE_X, SIZE_Y))

        self.panel_main = wx.Panel(self.sw, style=wx.SUNKEN_BORDER)

        self.chart_panel = PlotPanel(self.sw)

        self.sw.SplitVertically(self.chart_panel, self.panel_main, 600)

        # кнопки, текст и поля
        self.status_bar = self.CreateStatusBar()
        self.status_bar.SetStatusText("init")

        self.init_but = wx.Button(self.panel_main, -1, "read data", size=(260, 20), pos=(10, 10))
        self.run_but = wx.Button(self.panel_main, -1, "run", size=(260, 20), pos=(10, 40))
        self.clear_but = wx.Button(self.panel_main, -1, "stop", size=(260, 20), pos=(10, 70))

        # all labels
        self.label_0 = wx.StaticText(self.panel_main, label=TEXT_COUNT_POPULATION, pos=(10, 100))
        self.label_1 = wx.StaticText(self.panel_main, label=TEXT_COUNT_GENS, pos=(10, 130))
        self.label_1 = wx.StaticText(self.panel_main, label=TEXT_PROBABILITY_COSING_OVER, pos=(10, 130))
        self.label_2 = wx.StaticText(self.panel_main, label=TEXT_PROBABILITY_MUTATE, pos=(10, 160))
        self.label_3 = wx.StaticText(self.panel_main, label=TEXT_COUNT_GENERATION, pos=(10, 190))
        self.label_4 = wx.StaticText(self.panel_main, label=TEXT_BEST, pos=(10, 220))
        self.label_5 = wx.StaticText(self.panel_main, label=TEXT_CITIES_LIST, pos=(10, 250))

        self.editCountPopulation = wx.TextCtrl(self.panel_main, size=(80, 20), pos=(190, 100))
        self.editProbabilityCrossingOver = wx.TextCtrl(self.panel_main, size=(80, 20), pos=(190, 130))
        self.editProbabilityMutate = wx.TextCtrl(self.panel_main, size=(80, 20), pos=(190, 160))
        self.editCountGenerations = wx.TextCtrl(self.panel_main, size=(80, 20), pos=(190, 190))
        self.editFunction = wx.TextCtrl(self.panel_main, size=(80, 20), pos=(190, 220))

        # Set event handlers
        self.init_but.Bind(wx.EVT_BUTTON, self.init)
        self.run_but.Bind(wx.EVT_BUTTON, self.run)
        self.clear_but.Bind(wx.EVT_BUTTON, self.clear)

        self.cities_list = wx.ListCtrl(self.panel_main, size=(-1, 480), style=wx.LC_REPORT | wx.ALWAYS_SHOW_SB,
                                       pos=(10, 280))
        self.cities_list.InsertColumn(col=0, heading="node", format=wx.LIST_FORMAT_CENTER, width=234)

        self.init_options()  # установка начальных параметров
        self.Show()

    def __del__(self):
        pass

    def init_options(self):
        self.status_bar.SetStatusText("initialise start parameters")

        self.editCountPopulation.SetValue(str(POP_SIZE))
        self.editProbabilityCrossingOver.SetValue(str(CROSS_RATE))
        self.editProbabilityMutate.SetValue(str(MUTATION_RATE))
        self.editCountGenerations.SetValue(str(N_GENERATIONS))

        # очищаем список
        self.cities_list.ClearAll()
        self.cities_list.InsertColumn(col=0, heading="node", format=wx.LIST_FORMAT_CENTER, width=234)

    def draw(self, e):
        self.status_bar.SetStatusText("draw.. ")

    def init(self, e):
        self.status_bar.SetStatusText("init.. ")
        cities = self.open_file()
        self.chart_panel.plot(cities)

        self.pop_size = int(self.editCountPopulation.GetValue())
        self.mutate_rate = float(self.editProbabilityMutate.GetValue())
        self.cross_rate = float(self.editProbabilityCrossingOver.GetValue())
        self.n_generation = int(self.editCountGenerations.GetValue())
        self.end = False

        self.population = generate_init_population(cities, self.pop_size)

        f = self.calculate_fitness_F(self.population, cities)
        f_min = min(f)
        f_average = sum(f) / len(f)

        self.F_mean, self.F_min = [], []
        self.F_min.append(f_min)
        self.F_mean.append(f_average)

        pos_f_min = f.index(f_min)
        # print("F min position %d" % pos_f_min)

        best_way = self.population[pos_f_min]
        self.editFunction.SetValue(str(f_min))

        self.cities_list.ClearAll()
        self.cities_list.InsertColumn(col=0, heading="node", format=wx.LIST_FORMAT_CENTER, width=234)

        for j in range(len(best_way)):
            # self.cities_list.InsertStringItem(j, "text my")
            self.cities_list.InsertStringItem(j, str(best_way[j]))

        # рисуем лучший путь
        self.chart_panel.plot_way(cities, best_way)

        self.chart_panel.plot_best_and_average(f_min, f_average)
        self.cities = cities

        # показать лучшего
        # n_best = [i - 1 for i in best]
        # # self.population = []
        # self.population.append(n_best)
        # f = self.calculate_fitness_F(self.population, self.cities)
        # print(min(f))

    def run(self, e):
        self.n_generation -= 1
        self.status_bar.SetStatusText("processing.. ")
        old_f = 9999999.999
        count = 0

        while self.n_generation > 0:

            self.status_bar.SetStatusText("processing.. %s" % self.n_generation)

            children = get_children(self.population, cross_rate=self.cross_rate)
            # mutants = get_mutants(children, self.mutate_rate, self.cities)
            mutants = get_mutants(children + self.population, self.mutate_rate, self.cities)
            mutants_0 = get_mutants_0(children + self.population + mutants, self.mutate_rate, self.cities)

            self.population = selection_parents(self, self.population + children + mutants + mutants_0, self.cities, self.pop_size)

            # поиск лучших
            f = self.calculate_fitness_F(self.population, self.cities)
            f_min = min(f)
            f_average = sum(f) / len(f)
            pos_f_min = f.index(f_min)

            # self.editFunction.SetValue(str(f_min))      # отображаем пользователю минимаьный путь
            # print("F min position %d" % pos_f_min)
            self.F_min.append(f_min)
            self.F_mean.append(f_average)

            best_way = self.population[pos_f_min]

            # показываем лучший путь
            # self.cities_list.ClearAll()
            # self.cities_list.InsertColumn(col=0, heading="node", format=wx.LIST_FORMAT_CENTER, width=234)
            # for j in range(len(best_way)):
            #    self.cities_list.InsertStringItem(j, str(best_way[j]))

            # рисуем лучший путь
            # self.chart_panel.iterate_plot(self.cities, best_way)
            # self.chart_panel.plot_best_and_average_continuously(self.F_min, self.F_mean)

            # условия остановки или выхода из цикла
            if old_f != f_average:
                old_f = f_average
                count = 0
            else:
                if count == N_COUNTS:
                    break
                count += 1
            # уменьшаем итерации
            self.n_generation -= 1
            self.editCountGenerations.SetValue(str(self.n_generation))
            # time.sleep(0.5)

        self.editFunction.SetValue(str(f_min))  # отображаем пользователю минимаьный путь
        self.chart_panel.iterate_plot(self.cities, best_way)
        self.chart_panel.plot_best_and_average_continuously(self.F_min, self.F_mean)

        # пихаем в лист лучший путь
        self.cities_list.ClearAll()
        self.cities_list.InsertColumn(col=0, heading="node", format=wx.LIST_FORMAT_CENTER, width=234)
        for j in range(len(best_way)):
            self.cities_list.InsertStringItem(j, str(best_way[j] + 1))

    def clear(self, e):
        self.status_bar.SetStatusText("clear")
        self.init_options()  # установка начальных параметров

    def open_file(self):
        """ Open a file"""
        dir_name = ''
        dlg = wx.FileDialog(self, "Choose a file", dir_name, "", "*.tsp", wx.FD_OPEN)
        is_read = False
        if dlg.ShowModal() == wx.ID_OK:
            file_name = dlg.GetFilename()
            dir_name = dlg.GetDirectory()
            file_path = os.path.join(dir_name, file_name)
            if file_path != '':
                is_read = True
            print("file: " + file_path)
        dlg.Destroy()
        # если файл был прочтён
        if is_read:
            lines = []
            with open(file_path) as f:
                lines = f.readlines()
            # print(lines[3].replace(DIMENSION,''))
            # количество городов (генов)
            n_cities = int(lines[3].replace('DIMENSION :', ''))

            end_position = 0
            for i in range(len(lines)):
                if lines[i] == EOF:
                    end_position = i
                    break

            cities_data = lines[end_position - n_cities:end_position]
            cities = []
            for i in range(len(cities_data)):
                # n, x, y = cities_data[i].replace('\n', '').split()
                line = cities_data[i].replace('\n', '')
                n, x, y = line.split()

                cities.append((int(n) - 1, int(x), int(y)))
            print('cities', cities)
            # города в формате N-номер города X-координата города Y-координата города
            return cities

    def calculate_fitness_F(self, population, cities):
        f = []
        for j in range(len(population)):

            individ = population[j]
            distance = 0.0

            pos, x, y = get_pos_and_coordinate(cities)

            # всё кроме последнего перехода в начало пути
            for i in range(len(individ) - 1):
                n_A, n_B = individ[i], individ[i + 1]
                way = np.sqrt((x[n_B] - x[n_A]) ** 2 + (y[n_B] - y[n_A]) ** 2)
                # print("from %s" % n_A, "to %s" % n_B, "way %s" % way)
                # self.chart_panel.plot_line(x[n_A], x[n_B], y[n_A], y[n_B])
                distance += way
            # подсчёт от конца в начало
            n_A, n_B = individ[len(individ) - 1], individ[0]
            way = np.sqrt((x[n_B] - x[n_A]) ** 2 + (y[n_B] - y[n_A]) ** 2)
            # print("from end %s" % n_A, "to start %s" % n_B, "way %s" % way)
            # self.chart_panel.plot_line(x[n_A], x[n_B], y[n_A], y[n_B])
            distance += way
            f.append(distance)
        return f

    def show_message(self, message):
        dlg = wx.MessageDialog(self, message, "Information", wx.OK)  # создаём всплывашку
        dlg.ShowModal()  # показываем окошко


if __name__ == '__main__':
    app = wx.App(False)
    frame = GUI(None).Show(True)
    app.MainLoop()
