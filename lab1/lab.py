import time
import itertools
import random
import wx
import wx.xrc

from matplotlib.backends.backend_wxagg import FigureCanvasWxAgg as FigureCanvas
from matplotlib.backends.backend_wxagg import NavigationToolbar2WxAgg as NavigationToolbar
import matplotlib.pyplot as plt

import numpy as np

title = "Lab"
size_X = 900
size_Y = 800

text_count_population = "размер популяции, шт."
text_count_gens = "количество генов, шт."
text_probability_cosing_over = "вероятность кросинговера, %"
text_probability_mutate = "вероятность мутации, %"
text_count_generation = "макс. количество поколений, шт."
text_best_unit = "лучшея особь"
text_u = "t"
text_f = "f(t)"

# init all options
INIT_POPULATION_SIZE = 10
INIT_COUNT_GENS = 2
INIT_MUTATION_PROBABILITY = 0.1
INIT_CROSSING_OVER_PROBABILITY = 0.8  # вероятность скрещивание
INIT_MAX_ITERATION = 20

x, y = [], []
# constants
t_1 = -9.0
t_2 = 9.0
samples = 500

def f(x):  return (0.5 * x - 1.4) * np.cos(0.5 * np.pi * x + 1.1)


def set_bit(v, index, x):
    """
    Set the index: th bit of v to 1 if x is truthy, else to 0, and return the new value.
    :param v:
    :param index:
    :param x:
    :return:
    """
    mask = 1 << index  # Compute mask, an integer with just bit 'index' set.
    v &= ~mask  # Clear the bit indicated by the mask (if x is False)
    if x: v |= mask  # If x was True, set the bit indicated by the mask.
    return v  # Return the result, we're done.


def decision(probability):
    """
    функция рандома с вероятностью - probability
    :param probability:
    :return:
    """
    return random.random() < probability


def generate_init_population(population_size, count_gens):
    """
    Generates random population of gens
    :param population_size:
    :param count_gens:
    :return:
    """
    init_pop = []
    for _ in range(population_size):
        style = []
        for _ in range(count_gens):
            style.append(random.randint(0, 1))
        random.shuffle(style)
        init_pop.append(style)
    print(init_pop)
    return init_pop


def population_as_number(population, count_gens):
    pop = []
    for each in population:
        n = 0
        for i in range(count_gens):
            position = count_gens - i - 1
            bit = each[count_gens - position - 1]
            n = set_bit(n, position, bit)
        # print(n)
        pop.append(n)
    return pop


def unit_to_t(unit, count_gens=INIT_COUNT_GENS):
    """
    получение t на графике
    т.е. преобразование особи в точку t на графике цункций
    :param unit:
    :param count_gens: кол-во генов
    :return: в частном случае это число в пределах от -9.0 до 9.0
    """
    temp_population = []
    max_int = []
    for i in range(count_gens):
        max_int.append(1)
    temp_population.append(max_int)
    temp_population.append(unit)
    temp_population_as_numbers = population_as_number(temp_population, count_gens)
    t = ((temp_population_as_numbers[1]) / temp_population_as_numbers[0]) * (t_2 - t_1)
    result = t + t_1
    return result


def population_to_t(population, count_gens=INIT_COUNT_GENS):
    """
    получение t на графике
    т.е. преобразование популяции в точку t на графике цункций
    :param population:
    :param count_gens: кол-во генов
    :return: в частном случае это число в пределах от -9.0 до 9.0
    """
    pop = []
    for item in population:
        pop.append(unit_to_t(item, count_gens))
    return pop


def find_best(population, count_gens):
    """
    поиск лучшей оособи
    :param population:
    :param count_gens:
    :return: координаты лучшей остоби: t и f(t)
    """
    min_f = 99999999.0
    t_n = population_to_t(population, count_gens)
    list = []
    pos = 0
    for i in range(len(t_n)):
        result = f(t_n[i])
        list.append(result)
        if min_f > result:
            min_f = result
            pos = i
    best_t = t_n[pos]
    print("min f(t)=", min_f, " best t=", best_t)
    return best_t, min_f


def single_crossing_over(parent_1, parent_2, count_gens, probability=INIT_CROSSING_OVER_PROBABILITY):
    """
    скрещивание родительских генов
    :param parent_1:
    :param parent_2:
    :param probability:
    :return: возвращаем двух детей, если "получилиссь"
    """
    child_1, child_2 = [], []
    flag_1, flag_2 = False, False

    for i in range(count_gens):
        gen_1 = parent_1[i]
        gen_2 = parent_2[i]

        flag_1 = decision(probability=probability)
        flag_2 = decision(probability=probability)

        if flag_1:
            child_1.append(gen_1)
        else:
            child_1.append(gen_2)

        if flag_2:
            child_2.append(gen_1)
        else:
            child_2.append(gen_2)

    if flag_1 or flag_2: print(child_1, child_2)
    return child_1, child_2


def get_children(parents_population, population_size=INIT_POPULATION_SIZE, count_gens=INIT_COUNT_GENS,
                 probability=INIT_CROSSING_OVER_PROBABILITY):
    """
    скрещивание всех со всеми
    :param population:
    :param count_gens:
    :param probability:
    :return: дети от всех
    """
    children = []
    for i in range(population_size):
        for j in range(population_size):
            # print("скрещиваются все со всеми", "i:", i, "j", j)
            child_1, child_2 = single_crossing_over(parents_population[i], parents_population[j], count_gens,
                                                    probability)
            children.append(child_1)
            children.append(child_2)
    print("children", children)
    return list(children)


def single_mutate(unit, count_gens):
    """
    мутация одногоиз популяции
    :param unit:
    :param count_gens:
    :return: мутант с одной изменённой хромосомой
    """
    mutate_unit = []
    position = random.randint(0, count_gens)
    for i in range(count_gens):
        if i != position:
            mutate_unit.append(unit[i])
        else:
            gen = unit[i]
            if gen == 1:
                gen = 0
            elif gen == 0:
                gen = 1
            mutate_unit.append(gen)
    print("mutation")
    print(unit)
    print(mutate_unit)
    return mutate_unit


def get_new_generation(parents, population_size=INIT_POPULATION_SIZE, count_gens=INIT_COUNT_GENS,
                       probability_crossover=INIT_CROSSING_OVER_PROBABILITY,
                       probability_mutation=INIT_MUTATION_PROBABILITY):
    """
    новая совокупность осбей + удаляем лишних
    :param parent: родители
    :param probability_mutation: вероятность мутации
    :return:  родители + дети + мутанты
    """
    mutants = []
    for i in range(len(parents)):
        if decision(probability=probability_mutation):
            mutants.append(single_mutate(parents[i], count_gens))

    children = get_children(parents, population_size, count_gens, probability_crossover)

    all = list(itertools.chain(mutants, parents, children))

    unique_in_population = [list(x) for x in set(tuple(x) for x in all)]  # Uniqueness for list of lists

    # print(unique_data)
    return unique_in_population


def get_best_in_population(population, count_gens=INIT_COUNT_GENS, size_population=INIT_POPULATION_SIZE):
    """
    Выборка N лучши особей из всех
    :param population: подаём уникальных особей из всех возможных
    :param count_gens: кол-ыо генов
    :param size_population: кол-ыо особей - размер популяции
    :return: заданное кол-во популяции, вначале отсортированные далее копии самого лучшиего
    """
    loss = []
    best_population = []
    for each in population:
        t = unit_to_t(each, count_gens)
        loss.append(f(t))

    sorted_loss = sorted(loss)

    # print(loss)
    # print(sorted_loss)

    count = 0
    count_best = 0
    for i in range(len(sorted_loss)):
        for j in range(len(loss)):
            if sorted_loss[i] == loss[j]:
                best_population.append(population[j])
                if count == 0:
                    count_best = j
                count = count + 1
                if count == size_population:
                    break
        if count == size_population:
            break
    if count < size_population:
        for i in range(size_population - len(best_population)):
            best_population.append(population[count_best])

    # print("best ", best_population)
    return best_population


class panel(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)
        self.figure = plt.figure()

        self.canvas = FigureCanvas(self, -1, self.figure)
        self.toolbar = NavigationToolbar(self.canvas)
        self.toolbar.Hide()

        self.default_size_x, self.default_size_y = self.figure.get_size_inches()[0] / 1 , self.figure.get_size_inches()[1] / 2

    def plot(self):
        ''' plot some random stuff '''

        self.figure.clear()

        ax = self.figure.add_subplot(111)
        ax.hold(True)
        t_1 = -9.0
        t_2 = 9.0
        samples = 500
        # нагенеренные данные для логистической регресии
        t = np.linspace(t_1, t_2, samples)

        ax.plot(t, f(t), 'r-')

        t_random = random.randint(-9, 9)
        x.append(t_random)
        y.append(f(t_random))

        for i in range(samples // 60):
            x.append(t[i * 60])
            y.append(f(t[i * 60]))
        ax.scatter(x, y, c='b')

        ax.grid(True)
        self.canvas.draw()

    def plot_init_function(self):
        self.figure.clear()
        ax = self.figure.add_subplot(111)
        ax.clear()
        #ax.hold(True)
        t = np.linspace(t_1, t_2, samples)
        ax.plot(t, f(t), 'g-')
        # устанавливаем границы
        ax.set_xlim(left=t_1, right=t_2)
        ax.set_ylim(-6.0, 6.0)
        ax.grid(True)
        self.canvas.draw()

    def plot_function_all_population_and_best_of_population(self, X, Y, best_x, best_y):
        self.figure.clear()
        ax = self.figure.add_subplot(111)
        ax.set_ylabel('f(t)', fontsize='medium')  # relative to plt.rcParams['font.size']
        ax.set_xlabel('t', fontsize='medium')  # relative to plt.rcParams['font.size']
        # setting label sizes after creation
        ax.xaxis.label.set_size(14)
        ax.yaxis.label.set_size(14)
        # устанавливаем границы
        ax.set_xlim(left=t_1, right=t_2)
        ax.set_ylim(-6.0, 6.0)
        ax.clear()   #ax.hold(True)
        t = np.linspace(t_1, t_2, samples)
        ax.plot(t, f(t), color='orange')

        ax.scatter(X, Y, c='y', alpha=0.5)

        ax.scatter(best_x, best_y, c='r', alpha=0.5)

        ax.grid(True)
        self.canvas.draw()

    def plot_best_and_average(self, X, Y, best_x, best_y):

        self.figure.set_size_inches( (self.default_size_x, self.default_size_y) )

        self.figure.clear()
        ax = self.figure.add_subplot(111)
        ax.set_title("Best and Average")
        # устанавливаем границы
        ax.set_xlim(left=t_1, right=t_2)
        ax.set_ylim(-6.0, 6.0)
        ax.grid(True)   #Обычные шкалы

        ax.set_ylabel('f(t)', fontsize='medium')  # relative to plt.rcParams['font.size']
        ax.set_xlabel('t', fontsize='medium')  # relative to plt.rcParams['font.size']
        # setting label sizes after creation
        ax.xaxis.label.set_size(14)
        ax.yaxis.label.set_size(14)
        #ax.hold(True)

        avr_t, sum_t = 0.0, 0.0
        for each in X:
            sum_t = sum_t + each
        avr_t = sum_t / len(X)

        sum_r = 0.0
        for each in Y:
            sum_r = sum_r + each
        avr_r = sum_r / len(Y)

        ax.scatter(avr_t, avr_r, c='y')

        ax.scatter(best_x, best_y, c='r')

        ax.grid(True)
        self.canvas.draw()


class MyForm(wx.Frame):
    def __init__(self, parent):

        self.population = []
        self.population_size = 10
        self.count_gens = 2
        self.mutation_probability = 0.1
        self.crossing_over_probability = 0.8  # вероятность скрещивание
        self.iteration = 100
        self.end_flag = False

        self.average_x = 0.0
        self.average_y = 0.0

        no_resize = wx.SYSTEM_MENU | wx.SYSTEM_MENU | wx.CAPTION | wx.CLOSE_BOX

        wx.Frame.__init__(self, None, title=title, size=wx.Size(size_X, size_Y), style=no_resize)

        self.sw = wx.SplitterWindow(self)

        self.SetMinSize((size_X, size_Y))
        self.SetMaxSize((size_X, size_Y))

        self.sw_0 = wx.SplitterWindow(self.sw)
        self.sw_1 = wx.SplitterWindow(self.sw)

        self.panel_main = wx.Panel(self.sw_0, style=wx.SUNKEN_BORDER)
        self.panel_second = wx.Panel(self.sw_1, style=wx.SUNKEN_BORDER)

        self.sw.SplitHorizontally(self.sw_0, self.sw_1, 480)

        self.chart_panel_0 = panel(self.sw_0)
        self.chart_panel_1 = panel(self.sw_1)

        self.sw_0.SplitVertically(self.chart_panel_0, self.panel_main, 600)
        self.sw_1.SplitVertically(self.chart_panel_1, self.panel_second, 600)

        self.status_bar = self.CreateStatusBar()
        self.status_bar.SetStatusText("init")

        self.init_but = wx.Button(self.panel_main, -1, "init", size=(260, 20), pos=(10, 10))
        self.run_but = wx.Button(self.panel_main, -1, "run", size=(260, 20), pos=(10, 40))
        self.clear_but = wx.Button(self.panel_main, -1, "clear", size=(260, 20), pos=(10, 70))

        # all labels
        self.label_0 = wx.StaticText(self.panel_main, label=text_count_population, pos=(10, 100))
        self.label_1 = wx.StaticText(self.panel_main, label=text_count_gens, pos=(10, 130))
        self.label_2 = wx.StaticText(self.panel_main, label=text_probability_cosing_over, pos=(10, 160))
        self.label_3 = wx.StaticText(self.panel_main, label=text_probability_mutate, pos=(10, 190))
        self.label_4 = wx.StaticText(self.panel_main, label=text_count_generation, pos=(10, 220))
        self.label_5 = wx.StaticText(self.panel_main, label=text_best_unit, pos=(110, 250))
        self.label_6 = wx.StaticText(self.panel_main, label=text_u, pos=(10, 280))
        self.label_7 = wx.StaticText(self.panel_main, label=text_f, pos=(10, 310))

        self.editCountPopulation = wx.TextCtrl(self.panel_main, size=(80, 20), pos=(190, 100))
        self.editGens = wx.TextCtrl(self.panel_main, size=(80, 20), pos=(190, 130))
        self.editProbabilityCrossingOver = wx.TextCtrl(self.panel_main, size=(80, 20), pos=(190, 160))
        self.editProbabilityMutate = wx.TextCtrl(self.panel_main, size=(80, 20), pos=(190, 190))
        self.editCountGenerations = wx.TextCtrl(self.panel_main, size=(80, 20), pos=(190, 220))

        self.editUnit = wx.TextCtrl(self.panel_main, size=(80, 20), pos=(190, 280))
        self.editFunction = wx.TextCtrl(self.panel_main, size=(80, 20), pos=(190, 310))

        # Set event handlers
        self.init_but.Bind(wx.EVT_BUTTON, self.init)
        self.run_but.Bind(wx.EVT_BUTTON, self.run)
        self.clear_but.Bind(wx.EVT_BUTTON, self.clear)

        self.Show()

    def draw(self, e):
        self.status_bar.SetStatusText("draw.. ")
        self.chart_panel_0.plot()

    def init(self, e):
        self.status_bar.SetStatusText("init.. ")
        self.clear(e)
        self.population_size = int(self.editCountPopulation.GetValue())
        self.count_gens = int(self.editGens.GetValue())
        self.mutation_probability = float(self.editProbabilityCrossingOver.GetValue())
        self.crossing_over_probability = float(self.editProbabilityMutate.GetValue())
        self.iteration = int(self.editCountGenerations.GetValue())

        self.population = generate_init_population(self.population_size, self.count_gens)
        population_t = population_to_t(self.population, self.count_gens)

        b_t, b_f = find_best(self.population, self.count_gens)
        # отображаем пользвателю
        self.editUnit.SetValue(str(b_t))
        self.editFunction.SetValue(str(b_f))

        y = []
        for item in population_t:
            temp = f(item)
            y.append(temp)
            print("t=", item, "f(t)=", temp)
        self.chart_panel_0.plot_function_all_population_and_best_of_population(population_t, y, b_t, b_f)


    def run(self, e):


        if self.end_flag == True:
            self.show_message("End")
        else:
            count = 0
            old = 0
            for i in range(self.iteration):
                self.status_bar.SetStatusText("осталось итераций" + str(self.iteration))
                #self.iteration = self.iteration - 1
                new_population = get_new_generation(self.population, self.population_size, self.count_gens, self.crossing_over_probability, self.mutation_probability)
                new_population = get_best_in_population(new_population, count_gens=self.count_gens, size_population=self.population_size)

                population_t = population_to_t(new_population, self.count_gens)

                b_t, b_f = find_best(new_population, self.count_gens)
                # отображаем пользвателю
                self.editUnit.SetValue(str(b_t))
                self.editFunction.SetValue(str(b_f))

                y = []
                for item in population_t:
                    temp = f(item)
                    y.append(temp)
                    print("t=", item, "f(t)=", temp)

                self.chart_panel_0.plot_function_all_population_and_best_of_population(population_t, y, b_t, b_f)
                self.population = new_population

                #для второго графика отображение
                self.chart_panel_1.plot_best_and_average(population_t, y, b_t, b_f)

                if old == b_f:
                    count = count +1
                else:
                    count = 0
                old = b_f
                time.sleep(1)
                if count == 5:
                    self.end_flag = True
                    break
            self.show_message("End")


    def clear(self, e):
        """
        очистка переменных и стирание данных  в полях
        :param e:
        :return:
        """
        self.status_bar.SetStatusText("clear")
        self.population_size = INIT_POPULATION_SIZE
        self.count_gens = INIT_COUNT_GENS
        self.mutation_probability = INIT_MUTATION_PROBABILITY
        self.crossing_over_probability = INIT_CROSSING_OVER_PROBABILITY  # вероятность скрещивание
        self.iteration = INIT_MAX_ITERATION

        self.editCountPopulation.SetValue(str(self.population_size))
        self.editGens.SetValue(str(self.count_gens))
        self.editProbabilityCrossingOver.SetValue(str(self.crossing_over_probability))
        self.editProbabilityMutate.SetValue(str(self.mutation_probability))
        self.editCountGenerations.SetValue(str(self.iteration))

        population = []
        self.chart_panel_0.plot_init_function()

    def show_message(self, message):
        dlg = wx.MessageDialog(self, message, "Information", wx.OK)  # создаём всплывашку
        dlg.ShowModal()  # показываем окошко

    def __del__(self):
        pass


if __name__ == '__main__':
    app = wx.App(False)
    frame = MyForm(None).Show(True)
    # frame.Show(True)
    app.MainLoop()
